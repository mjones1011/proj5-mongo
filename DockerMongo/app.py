import os
import sys
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow
import acp_times
import logging


app = Flask(__name__)
app.secret_key = 'SHEmGORhDfCRyisd8N8nEbFA'

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
db.tododb.delete_many({}) #Makes sure database is clear

##COPIED IN FROM FLASK_BREVETS.PY

#####
#Sets up homepage
#####
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')

#####
#Calculates times
#####
@app.route("/_calc_times")
def calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request: km")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))

    app.logger.debug("Got a JSON request: distance")
    distance = request.args.get('brevet_distance_km', 999, type=int)
    app.logger.debug("distance={}".format(distance))

    app.logger.debug("Got a JSON request: begin_date")
    begin_date = request.args.get('begin_date', '', type=str)
    app.logger.debug("begin_date={}".format(begin_date))

    app.logger.debug("Got a JSON request: begin_time")
    begin_time = request.args.get('begin_time', '', type=str)
    app.logger.debug("begin_time={}".format(begin_time))

    app.logger.debug("request.args: {}".format(request.args))

    fulltime = begin_date + " " + begin_time
    time = arrow.get(fulltime, 'YYYY-MM-DD HH:mm')

    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#####
#Spits 404
#####
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return render_template('404.html'), 404
##END COPY IN FROM FLASK_BREVETS.PY

@app.route('/display')
def display():
    
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    kilometers = request.form.getlist('km')
    starttimes = request.form.getlist('open')
    endtimes = request.form.getlist('close')
    
    for item in range(20):
        if item != '': #should always be true
            finalinfo = {
                'km': kilometers[item],
                'open_time': starttimes[item],
                'close_time': endtimes[item]
            }
        db.tododb.insert_one(finalinfo)

    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
