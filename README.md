# Project 5: Brevet time calculator with Ajax and MongoDB
Author: Mason Jones

Contact: masonj@uoregon.edu

Use: To calculate ACP Brevet times.


Instructions:

1) Choose overall distance.

2) Input checkpoints as km or miles, but DO NOT HIT ENTER. CLICK INTO ANOTHER FIELD TO AUTOCOMPLETE TIMES. HITTING ENTER WILL ACT AS A "SUBMIT" BUTTON.

3) Hit the submit  button to enter your data into the database.

4) Hit "Display Times" to see your brevet information.


BREVET RULES:

While <200km, max speed = 34, min speed = 15

200km < n <= 400km, max speed = 32, min speed = 15

400km < n <= 600km, max speed = 30, min speed = 15

600km < n <= 1000km, max speed = 28, min speed = 11.428
